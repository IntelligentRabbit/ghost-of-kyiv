from random import randint
from functools import partial

from kivymd.app import MDApp
from kivymd.uix.screen import MDScreen
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen, FadeTransition
from kivy.uix.widget import Widget
from kivy.core.text import LabelBase
from kivy.clock import Clock
from kivy.animation import Animation
from kivy.core.audio import SoundLoader
from kivy.properties import NumericProperty, ObjectProperty, BooleanProperty

from background import Background
from objects import Enemy, Bullet, Fuel, RepairKit, Explosion
from file_screen import FileScreen
from welcome_screen import WelcomeScreen
from io_data import save, load

Builder.load_file("objects.kv")
Builder.load_file("progressbar.kv")
Builder.load_file("file_screen.kv")
Builder.load_file("welcome_screen.kv")


class Game(ScreenManager):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.sound_explosion = SoundLoader.load("sounds/explosion.wav")
        self.sound_notification = SoundLoader.load(
            "sounds/mixkit-retro-game-notification-212.wav"
        )
        self.sound_horn = SoundLoader.load("sounds/mixkit-war-horn-ambience-2785.ogg")
        self.sound_cinematic = SoundLoader.load("sounds/mixkit-cinematic.ogg")
        self.sound_drums = SoundLoader.load("sounds/drums.ogg")

        self.sound_drums.loop = True
        self.sound_explosion.volume = 0.3
        self.sound_drums.volume = 1


class PlayScreen(Screen):
    play_widget = ObjectProperty(None)

    def on_enter(self):
        self.play_widget.start()

    def on_leave(self):
        self.play_widget.reset()


class GameOver(MDScreen):
    mission_failed = BooleanProperty(False)
    num_destroyed = NumericProperty(0)

    def on_enter(self):
        self.mission_failed = self.game.get_screen(
            "play_screen"
        ).play_widget.mission_failed
        self.num_destroyed = self.game.get_screen(
            "play_screen"
        ).play_widget.num_destroyed

        anim = Animation(font_size=44, duration=0.3)
        anim.start(self.ids.info_label)

    def on_leave(self):
        self.ids.info_label.font_size = 10


class PlayWidget(Widget):
    bg = ObjectProperty(None)
    player = ObjectProperty(None)
    label = NumericProperty(0)
    weapon_reload = BooleanProperty(False)
    mission_failed = BooleanProperty(False)
    pb_health = NumericProperty(0)
    pb_fuel = NumericProperty(0)
    player_bullets = []
    enemies = []
    enemies_bullets = []
    fuel_stock = []
    repair_kits = []
    explosions = []
    events = []
    num_destroyed = NumericProperty(0)
    total_destroyed = NumericProperty(0)
    num_missed = NumericProperty(0)
    num_missions_failed = NumericProperty(0)
    num_missions_completed = NumericProperty(0)

    def start(self):
        self.reset()
        saves = load()
        self.total_destroyed = saves[0]
        self.num_missions_completed = saves[1]
        self.num_missions_failed = saves[2]

        event = Clock.schedule_interval(self.update, 1 / 90)
        event2 = Clock.schedule_interval(self.spawn_enemies, 4)
        event3 = Clock.schedule_interval(self.enemy_shoot, 4)
        event4 = Clock.schedule_interval(self.spawn_fuel, 20)
        event5 = Clock.schedule_interval(self.spawn_repair_kit, 45)
        self.events = [event, event2, event3, event4, event5]

        self.on_touch_down = self.user_action
        GhostApp.game.sound_drums.play()

    def update(self, nap):
        self.bg.update(nap)
        self.player.update(self.width, nap)

        for bullet in self.player_bullets:
            bullet.move(nap)
            if bullet.y > (self.height + bullet.height):
                self.remove_widget(bullet)
                self.player_bullets.remove(bullet)

        for enemy in self.enemies:
            enemy.move(nap)
            if enemy.y <= -enemy.height:
                self.remove_widget(enemy)
                self.enemies.remove(enemy)
                self.num_missed += 1

        for bullet in self.enemies_bullets:
            bullet.move(nap)
            if bullet.y <= -bullet.height:
                self.remove_widget(bullet)
                self.enemies_bullets.remove(bullet)

        for fuel in self.fuel_stock:
            fuel.update(nap)
            if fuel.y <= -fuel.height:
                self.remove_widget(fuel)
                self.fuel_stock.remove(fuel)

        for rk in self.repair_kits:
            rk.update(nap)
            if rk.y <= -rk.height:
                self.remove_widget(rk)
                self.repair_kits.remove(rk)

        for exp in self.explosions:
            exp.update(nap)
            if exp.y <= -exp.height:
                self.remove_widget(exp)
                self.explosions.remove(exp)

        if len(self.player_bullets) == 0:
            self.weapon_reload = False
        self.pb_fuel = int(self.player.fuel)
        self.pb_health = self.player.health

        self.check_collisions()

        if self.test_game_over():
            self.game_over()

    def user_action(self, touch, *args):
        if self.player.collide_point(touch.x, touch.y) and len(self.player_bullets) < 2:
            self.player_shoot()
        if not self.player.collide_point(touch.x, touch.y):
            if touch.x < self.player.x:
                self.player.direction = "left"
            if touch.x > self.player.x:
                self.player.direction = "right"

    def on_touch_up(self, touch):
        self.player.direction = ""

    def player_shoot(self):
        bullet1 = Bullet(
            self.player.x + 15,
            self.player.y + 100,
            "player",
        )
        self.add_widget(bullet1)
        self.player_bullets.append(bullet1)
        bullet2 = Bullet(
            self.player.x + 120,
            self.player.y + 100,
            "player",
        )
        self.add_widget(bullet2)
        self.player_bullets.append(bullet2)
        self.weapon_reload = True

    def spawn_enemies(self, dt):
        spawn_x = randint(100, self.width - 100)
        spawn_y = self.height
        enemy = Enemy(x=spawn_x, y=spawn_y)
        self.add_widget(enemy)
        self.enemies.append(enemy)

    def enemy_shoot(self, dt):
        if len(self.enemies) != 0:
            x = self.enemies[-1].x + 100
            y = self.enemies[-1].y + 100
            bullet = Bullet(x, y, "enemy")
            self.add_widget(bullet)
            self.enemies_bullets.append(bullet)

    def spawn_fuel(self, dt):
        spawn_x = randint(100, self.width - 100)
        spawn_y = self.height
        fuel = Fuel(x=spawn_x, y=spawn_y)
        self.add_widget(fuel)
        self.fuel_stock.append(fuel)

    def spawn_repair_kit(self, dt):
        spawn_x = randint(100, self.width - 100)
        spawn_y = self.height
        rk = RepairKit(x=spawn_x, y=spawn_y)
        self.add_widget(rk)
        self.repair_kits.append(rk)

    def get_explosion(self, x_exp, y_exp):
        exp = Explosion(x=x_exp, y=y_exp)
        self.add_widget(exp)
        self.explosions.append(exp)

        def change_explosion(img, dt):
            img += 1
            exp.image = exp.images_list[img]

        for i in range(4):
            Clock.schedule_once(partial(change_explosion, i + 1), (0.2 + i * 0.15))

    def check_collisions(self):
        for enemy in self.enemies:
            for bullet in self.player_bullets:
                if enemy.collide_point(
                    bullet.x + bullet.width / 2, bullet.y + bullet.height
                ):
                    self.remove_widget(bullet)
                    self.player_bullets.remove(bullet)
                    self.remove_widget(enemy)
                    self.enemies.remove(enemy)
                    self.get_explosion(enemy.x, enemy.y)
                    GhostApp.game.sound_explosion.play()
                    self.num_destroyed += 1

        for bullet in self.enemies_bullets:
            if bullet.collide_widget(self.player):
                self.remove_widget(bullet)
                self.enemies_bullets.remove(bullet)
                self.get_explosion(
                    self.player.x, self.player.y + self.player.height / 2
                )
                self.player.health -= 100
                GhostApp.game.sound_explosion.play()

        for fuel in self.fuel_stock:
            if fuel.collide_widget(self.player):
                self.remove_widget(fuel)
                self.fuel_stock.remove(fuel)
                GhostApp.game.sound_notification.play()
                if self.player.fuel <= 90:
                    self.player.fuel += 15
                else:
                    self.player.fuel = 100

        for rk in self.repair_kits:
            if rk.collide_widget(self.player):
                self.remove_widget(rk)
                self.repair_kits.remove(rk)
                GhostApp.game.sound_notification.play()
                if self.player.health <= 200:
                    self.player.health += 100
                else:
                    self.player.health = 300

    def test_game_over(self):
        if self.player.health == 0 or self.player.fuel <= 0:
            self.mission_failed = True
            self.num_missions_failed += 1
            return True

        elif (self.num_destroyed + self.num_missed) == 50:
            self.mission_failed = False
            self.num_missions_completed += 1
            return True

        return False

    def reset(self):
        self.player.reset()
        self.player.pos = (self.width / 2 - self.player.width / 2, self.height * 0.1)
        self.num_destroyed = 0
        self.num_missed = 0

        if len(self.player_bullets) != 0:
            for bullet in self.player_bullets:
                self.remove_widget(bullet)
                self.player_bullets.remove(bullet)

        if len(self.enemies) != 0:
            for enemy in self.enemies:
                self.remove_widget(enemy)
                self.enemies.remove(enemy)

        if len(self.enemies_bullets) != 0:
            for bullet in self.enemies_bullets:
                self.remove_widget(bullet)
                self.enemies_bullets.remove(bullet)

        if len(self.fuel_stock) != 0:
            for fuel in self.fuel_stock:
                self.remove_widget(fuel)
                self.fuel_stock.remove(fuel)

        if len(self.repair_kits) != 0:
            for rk in self.repair_kits:
                self.remove_widget(rk)
                self.repair_kits.remove(rk)

        if len(self.explosions) != 0:
            for exp in self.explosions:
                self.remove_widget(exp)
                self.explosions.remove(exp)

    def game_over(self):
        for event in self.events:
            event.cancel()
        GhostApp.game.sound_drums.stop()
        self.total_destroyed += self.num_destroyed
        save(
            self.total_destroyed, self.num_missions_completed, self.num_missions_failed
        )
        GhostApp.game.current = "gameover_screen"


class GhostApp(MDApp):
    def build(self):
        self.theme_cls.theme_style = "Dark"
        GhostApp.game = Game(transition=FadeTransition())
        self.game.add_widget(WelcomeScreen(name="welcome_screen"))
        self.game.add_widget(PlayScreen(name="play_screen"))
        self.game.add_widget(FileScreen(name="file_screen"))
        self.game.add_widget(GameOver(name="gameover_screen"))
        return self.game


if __name__ == "__main__":
    LabelBase.register(
        name="Rubik",
        fn_regular="fonts/Rubik-Regular.ttf",
        fn_bold="fonts/Rubik-Bold.ttf",
        fn_italic="fonts/armalite_rifle.ttf",
    )
    GhostApp().run()
