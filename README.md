# Ghost of Kyiv :blue_heart: :yellow_heart:

Shoot 'em up game App for Android.

Darkness covered the world. The evil, that grew in the east, began to spread like a cancer. Putin's minions are tearing Ukraine apart. In the middle of the night's darkness, he appears, the one who protects civilians from ruscist's bombs, the one who knows neither peace nor pity, while this war continues, the eternal avenger and tireless defender - Ghost of Kyiv.

Watch gameplay  [on YouTube.](https://youtu.be/X_ktXkHJOME)


# Prerequisites
- [Python 3.6+](https://www.python.org/)
- [Kivy>=2.0.0](https://kivy.org/#home)
- [KivyMD>=0.104.2](https://kivymd.readthedocs.io/en/latest/)
