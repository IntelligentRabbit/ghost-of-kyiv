from kivymd.uix.screen import MDScreen
from kivy.properties import NumericProperty, StringProperty


class FileScreen(MDScreen):
    aerial_victories = NumericProperty(0)
    missions_completed = NumericProperty(0)
    missions_failed = NumericProperty(0)
    battle_efficency = NumericProperty(0)

    award1_image = StringProperty("")
    award2_image = StringProperty("")
    award3_image = StringProperty("")
    award4_image = StringProperty("")
    award5_image = StringProperty("")

    award1_condition = StringProperty("")
    award2_condition = StringProperty("")
    award3_condition = StringProperty("")
    award4_condition = StringProperty("")
    award5_condition = StringProperty("")

    def on_enter(self):
        self.award1_condition = "100 Aerial Victories"
        self.award2_condition = "3 Missions completed"
        self.award3_condition = "500 Aerial Victories\n" + "Battle Efficiency > 50%"
        self.award4_condition = "10 Missions completed\n" + "Battle Efficiency > 50%"
        self.award5_condition = (
            "1000 Aerial Victories\n"
            + "20 Missions completed\n"
            + "Battle Efficiency > 70%"
        )

        self.aerial_victories = self.game.get_screen(
            "play_screen"
        ).play_widget.total_destroyed
        self.missions_completed = self.game.get_screen(
            "play_screen"
        ).play_widget.num_missions_completed
        self.missions_failed = self.game.get_screen(
            "play_screen"
        ).play_widget.num_missions_failed

        self.calc_battle_efficiency()
        self.unlock_awards()
        self.ids.swiper.set_current(0)

    def calc_battle_efficiency(self):
        total_missions = self.missions_completed + self.missions_failed
        if total_missions > 0:
            self.battle_efficency = round(self.missions_completed * 100 / total_missions)
        else:
            self.battle_efficency = 0

    def unlock_awards(self):
        if self.aerial_victories >= 100:
            self.award1_image = "images/awards/award-1.png"
        else:
            self.award1_image = "images/awards/award-1-lock.png"

        if self.missions_completed >= 3:
            self.award2_image = "images/awards/award-2.png"
        else:
            self.award2_image = "images/awards/award-2-lock.png"

        if self.aerial_victories >= 500 and self.battle_efficency > 50:
            self.award3_image = "images/awards/award-3.png"
        else:
            self.award3_image = "images/awards/award-3-lock.png"

        if self.missions_completed >= 10 and self.battle_efficency > 50:
            self.award4_image = "images/awards/award-4.png"
        else:
            self.award4_image = "images/awards/award-4-lock.png"

        if (
            self.aerial_victories >= 100
            and self.missions_completed >= 20
            and self.battle_efficency > 70
        ):
            self.award5_image = "images/awards/award-5.png"
        else:
            self.award5_image = "images/awards/award-5-lock.png"
