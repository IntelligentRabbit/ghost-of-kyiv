import json


def save(var1, var2, var3):
    to_json = {
        "var1": var1,
        "var2": var2,
        "var3": var3,
    }
    with open("saves.json", "w") as f:
        json.dump(to_json, f)


def load():
    vars = []
    try:
        with open("saves.json", "r") as f:
            from_json = json.load(f)
        for i in range(3):
            i = from_json[f"var{i+1}"]
            vars.append(i)
    except FileNotFoundError:
        for i in range(3):
            i = 0
            vars.append(i)
    return vars
