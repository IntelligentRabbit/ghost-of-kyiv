from kivymd.uix.screen import MDScreen
from kivy.animation import Animation
from kivy.uix.image import Image
from kivy.properties import NumericProperty


class F22(Image):
    angle = NumericProperty(22)
    
    
class WelcomeScreen(MDScreen):
    def on_enter(self):
        anim = Animation(opacity=0, duration=1) + Animation(opacity=1, duration=1)
        anim.repeat = True
        anim.start(self.ids.tap_label)
        
        anim2 = Animation(size=(496, 305), duration=3)
        anim2 &= Animation(angle=0, duration=3)
        anim2.start(self.ids.f22_image)
        
        anim3 = Animation(size_hint=(1, 1), duration=4)
        anim3.start(self.ids.flag_image)
        
        self.game.sound_horn.play()
        self.game.sound_cinematic.play()

    def on_touch_down(self, touch):
        self.game.current = "play_screen"
        self.game.sound_horn.stop()
        self.game.sound_cinematic.stop()
