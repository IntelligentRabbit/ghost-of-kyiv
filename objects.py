from random import randint

from kivy.uix.image import Image as ImageWidget
from kivy.properties import StringProperty, NumericProperty


class PlayerFighter(ImageWidget):
    direction = StringProperty("")
    health = NumericProperty(300)
    fuel = NumericProperty(100)

    def update(self, width, nap):
        if self.direction == "":
            self.x = self.x
        elif self.direction == "left":
            if self.x > width * 0.1 - 60:
                self.x -= 200 * nap
        elif self.direction == "right":
            if self.x + self.width < width * 0.9 + 60:
                self.x += 200 * nap

        self.fuel -= 1 * nap

    def reset(self):
        self.health = 300
        self.fuel = 100


class Bullet(ImageWidget):
    speed = NumericProperty(0)
    type_image = StringProperty("")

    def __init__(self, x, y, type_):
        super(Bullet, self).__init__()
        self.x = x
        self.y = y
        self.type = type_
        if type_ == "player":
            self.type_image = "images/bullet-1.png"
            self.speed = 360
        if type_ == "enemy":
            self.type_image = "images/bullet-2.png"
            self.speed = -600

    def move(self, nap):
        self.y += self.speed * nap


class Enemy(ImageWidget):
    type = NumericProperty(0)
    type_image = StringProperty("")

    def __init__(self, **kwargs):
        super(Enemy, self).__init__(**kwargs)
        self.type = randint(0, 3)
        for i in range(4):
            if self.type == i:
                self.type_image = f"images/enemies/enemy-{i+1}.png"

        if self.type == 3:
            self.size = (180, 180)
        if self.type == 0:
            self.size = (160, 220)

    def move(self, nap):
        self.y -= 360 * nap


class Fuel(ImageWidget):
    def update(self, nap):
        self.y -= 100 * nap


class RepairKit(ImageWidget):
    def update(self, nap):
        self.y -= 100 * nap


class FighterIcon(ImageWidget):
    pass


class Explosion(ImageWidget):
    image = StringProperty("")
    images_list = []

    def __init__(self, **kwargs):
        super(Explosion, self).__init__(**kwargs)
        for i in range(6):
            self.image = f"images/explosion/{i+1}.png"
            self.images_list.append(self.image)
        self.image = f"images/explosion/1.png"

    def update(self, nap):
        self.y -= 100 * nap
