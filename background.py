from kivy.core.image import Image
from kivy.uix.widget import Widget
from kivy.properties import ObjectProperty


class BaseWidget(Widget):
    def load_tileable(self, name):
        t = Image(f"images/{name}.png").texture
        t.wrap = "repeat"
        setattr(self, f"tx_{name}", t)


class Background(BaseWidget):
    tx_bg = ObjectProperty(None)
    # tx_cloud = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(Background, self).__init__(**kwargs)
        self.load_tileable("bg")

    def set_background_size(self, tx):
        tx.uvsize = (1, self.height / tx.height)

    def on_size(self, *args):
        self.set_background_size(self.tx_bg)

    def set_background_uv(self, name, val):
        t = getattr(self, name)
        t.uvpos = (t.uvpos[0], (t.uvpos[1] - val) % self.height)
        self.property(name).dispatch(self)

    def update(self, nap):
        self.set_background_uv("tx_bg", 0.1 * nap)
        # self.set_background_uv("tx_grass", 0)
